package com.jiten.innovations.springCloud.springCloudSleuth.controller;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jiten.innovations.springCloud.springCloudSleuth.feignClients.TollRateServicesFeignClient;
import com.jiten.innovations.springCloud.springCloudSleuth.springcloudsleuth.SpringCloudSleuthApplication;

@RestController
public class SpringCloudSleuthDemoController {

	private static final Logger LOG = Logger.getLogger(SpringCloudSleuthDemoController.class.getName());
	
	@Autowired
	TollRateServicesFeignClient tollRateServicesFeignClientObject;
	
	@RequestMapping("/tollRate-sleuth")
	public String getTollRateServices() {
		LOG.info("inside getTollRateServices ------- ");
		return tollRateServicesFeignClientObject.getTollRateFixed();
	}
	
	
}
