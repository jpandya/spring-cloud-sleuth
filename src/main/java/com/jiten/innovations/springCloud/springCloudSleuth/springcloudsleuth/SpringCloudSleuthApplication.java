package com.jiten.innovations.springCloud.springCloudSleuth.springcloudsleuth;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.jiten.innovations.springCloud.springCloudSleuth.controller"})
@EnableFeignClients(basePackageClasses = com.jiten.innovations.springCloud.springCloudSleuth.feignClients.TollRateServicesFeignClient.class)
public class SpringCloudSleuthApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringCloudSleuthApplication.class, args);
	}

}
