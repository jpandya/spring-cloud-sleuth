package com.jiten.innovations.springCloud.springCloudSleuth.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="tollRateServices",url = "localhost:8085")
public interface TollRateServicesFeignClient {
	
	@RequestMapping("/tollRate/fixed")
	public String getTollRateFixed();
	
	
	
}
